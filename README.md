# CMS Manager - Metalsmith GUI

### A Content Management solution for a Metalsmith Static Site

#### To-Do

* Create GUI interface - Create with React **DONE**
* Create API to interact with 'metalsmith/src' directory
    * title **DONE**
    * view **DONE**
    * images - add & delete **DONE**
    * Navigation Order - **DONE**
    * Description - **DONE**
    * Keywords - **DONE**
    * Add Open Graph data (inc Facebook, Twitter)
    * Generate AMP Pages #Possibly set in YAML or do automatically for all posts??

* Use node fs module to interact with metalsmith content in the 'metalsmith/src' directory
* A means to upload images and store logically relating to page/post
* use a hosting API to automatically deploy the Site (**Bitballoon** or **Firebase**)

***Start With Pages and Posts***

### Metalsmith Site Folder Structure

The Metalsmith Site id required to be in a '/site' folder in the root directory.

* /site
    * /css        #Takes .scss to be processed during the build
    * /images     #All images by default will be uploaded here
    * /pages      #Markdown files for the Pages of the Site - // Frontmatter example below
    * /posts      #Markdown files for the Posts of the Site
    * index.md    #This is the homepage file

#### Frontmatter

Frontmatter in the markdown files is written in YAML. As a minimum the title: & view: are required.

Frontmatter is generated automatically using User Submitted Data from the Editor Page of the CMS.

```yaml
---
title: My Page title
view: default.html #Template Layout to use for the page
description: '' #Meta Description for this page
keywords: '' #Meta Keywords for the page
date: '' #Will be used for posts
order: '1' #Navigation Order - Only for Pages
images: null #An Array of image objects {path: "my/image/path.jpg", alt: "My Image Alt Text"}
banner: null #An Image selected from image array above
---
```

#### To Install...

```bash
git clone https://forbes_gray@bitbucket.org/forbes_gray/metalsmithgui.git metalsmith-gui
cd metalsmith-gui && npm install
```

###### Contributors - forbesg
