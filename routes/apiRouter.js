const express = require('express');
const router = express();
const buildScript = require(__dirname + '/../admin/build.js');
const devServerScript = require(__dirname + '/../admin/server.js');
const bitballoonOptions = require('../config/bitballoon-client-options.json');

const bitballoonClient = require('../config/bitballoon-client');

// Get Access Token from bitballoon
bitballoonClient.getAccessToken();

// enable access to child process pid
let serverChildProcess = null;

//API
router.get('/', (req, res) => {
  res.send({
    message: "This is the route of our server api"
  });
});

// Build Site
router.post('/build', (req, res) => {
  // Need a seperate build script to avoid metalsmith-serve running unless it has bee requested
  // If it it currently using the port - the server will close.
  buildScript('./site/index.js', (err) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log('finished running site build script');
      res.sendStatus(200);
    }
  });
});

router.post('/deploy', (req, res) => {
  if ((!bitballoonOptions.client_id || bitballoonOptions.client_id === "") ||
      (!bitballoonOptions.client_secret || bitballoonOptions.client_secret === "")) {
    console.log('Error with bitballoonOptions - redirecting to bitballoon settings page')
    res.status(200).send({
      bitballoonSettings: true
    });
  } else {
    bitballoonClient.deploySite(response => {
      if (response.code === 404) {
        res.status(404).send({message: "Bitballoon Site ID Not Found - Please check the site ID"});
      } else {
        res.status(200).send(response);
      }
    });
  }
  // console.log(bitballoonOptions);
});


// Start/Stop Child Process Express Server - based on passing req.params :control (start or stop)
router.post('/serve/:control', (req, res) => {
  let control = req.params.control,
      stop = false;
  if (control === 'stop' || serverChildProcess) {
    if (serverChildProcess) serverChildProcess.kill();
    serverChildProcess = null;
    res.status(200);
  } else if (!serverChildProcess && control === "start") {
    serverChildProcess = devServerScript('./site/dev-server.js', (err, signal) => {
      if (err && !signal) {
        console.log('Error from server');
        res.sendStatus(500);
      } else if ('server has stopped', signal) {
        console.log(signal)
        res.sendStatus(200);
      }
    });
  }

  if (control !== "stop" && control !== "start") {
    res.sendStatus(404);
  }

});

module.exports = router;
