const bitballoon    = require('bitballoon');
const bitballoonOptions = require('../config/bitballoon-client-options.json');

bitballoonOptions.dir = __dirname + '/../site/build';

let client = bitballoon.createClient({
  client_id: bitballoonOptions.client_id,
  client_secret: bitballoonOptions.client_secret
});


module.exports = {
  getAccessToken: () => {
    if (!bitballoonOptions.access_token) {
      client.authorizeFromCredentials(function(err, access_token) {
        if (err) return console.log(err);
        bitballoonOptions.access_token = access_token;
      });
    }
  },
  deploySite: (cb) => {
    let message;
    if (!bitballoonOptions.access_token) {
      message = 'There is no access token for this site';
      return cb({message: message});
    }
    bitballoon.deploy(bitballoonOptions, (err, deploy) => {
      if (err) {
        return cb({
          message: "There was an error accessing Bitballoon - please check your Bitballoon Settings"
        });
      }
      const response = {
        message: 'Site has been successfully deployed to Bitballoon',
        url: deploy.url
      };
      cb(response);
    });
  }
}
