const Twig          = require('twig');
const express       = require('express');
const apiRouter     = require('./routes/apiRouter');
const pagesManager  = require('./admin/pages-manager.js');
const path          = require('path');
const sassMiddleware = require('node-sass-middleware');
const fs            = require('fs');
const bodyParser    = require('body-parser');
const multer        = require('multer');
const yamljs        = require('yamljs');
const helpers       = require('./admin/helpers');

let app = express();

const sassDir       = path.join(__dirname + '/assets');
const cssDir        = path.join(__dirname + '/assets');
const siteRoot      = path.join(__dirname + '/site');


// Multer Image Uploads Config
const storage       = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname + '/site/src/images/'));
  },
  filename: (req, file, cb) => {
    cb(null, helpers.imageNameFormat(file.originalname));
  }
});
const userStorage       = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname + '/site/src/images/user'));
  },
  filename: (req, file, cb) => {
    cb(null, 'user.jpg');
  }
});
const upload        = multer({storage: storage});
const userUpload    = multer({storage: userStorage});

let getFiles = pagesManager.getFiles;

const config = {
  port: process.env.PORT || 3000
}


app.set('views', __dirname + '/views');
app.set('view engine', 'twig');

app.use('/api', apiRouter);

app.get('/', (req, res) => {
  getFiles(data => {
    res.render('dashboard', {
      nav: {
        pages: data.pages,
        posts: data.posts
      }
    });
  });
});


// Edit Site Settings
app.get('/settings', (req, res) => {
  getFiles(data => {
    pagesManager.getSettings(settings => {
      res.render('editor', {
        siteSettings: true,
        nav: {
          pages: data.pages,
          posts: data.posts
        },
        config: settings
      });
    });
  });
});

app.post('/settings/:type', userUpload.single('image'), (req, res) => {
  if (req.params.type === 'site') {
    let siteSettings = req.body;
    siteSettings.image = `/images/user/${req.file.filename}`;
    pagesManager.saveSiteSettings(siteSettings, () => {
      res.redirect('/');
    });
  }
  if (req.params.type === 'server') {
    const serverSettings = req.body;
    pagesManager.saveServerSettings(serverSettings, () => {
      res.redirect('/');
    });
  }
});

// Edit Site Settings
app.get('/bitballoon-settings', (req, res) => {
  getFiles(data => {
    pagesManager.getBitballoonSettings(settings => {
      res.render('editor', {
        bitballoonSettings: true,
        nav: {
          pages: data.pages,
          posts: data.posts
        },
        config: settings
      });
    });
  });
});

app.post('/bitballoon-settings', bodyParser.urlencoded({extended: true}), (req, res) => {
  const bitballoonConfig = req.body
  pagesManager.saveBitballoonSettings(bitballoonConfig, () => {
    res.redirect('/');
  });
});


// Edit exiting pages/posts
app.get('/edit/:type/:file', (req, res) => {
  const fileName = req.params.file;
  const fileType = req.params.type;
  getFiles(data => {
    pagesManager.getFile(req.params.type, req.params.file, (fileData) => {
      // Convert YAML image data to JSON Array
      helpers.toObject(fileData, (object) => {
        object.path = path.join(siteRoot, `/src/${req.params.type}/${req.params.file}`);
        pagesManager.getViews((viewsArray) => {
          // Get available view templates {fileName: 'view.html', title: 'view title'}
          res.render('editor', {
            nav: {
              pages: data.pages,
              posts: data.posts
            },
            views: viewsArray,
            title: object.title,
            view: object.view,
            description: object.description,
            keywords: object.keywords,
            date: object.date,
            order: object.order,
            content: object.body,
            editing: true,
            type: req.params.type,
            file: req.params.file,
            path: object.path,
            images: object.images
          });
        });
      });
    });
  });
});

// Image Upload
app.post('/edit/:type/:file/image', upload.single('image'), (req, res) => {

  const type = req.params.type;
  const file = req.params.file;
  const thisPath = `/edit/${type}/${file}`;
  const filePath = path.join(siteRoot, `/src/${req.params.type}/${req.params.file}`)
  pagesManager.getFile(type, file, (content) => {
    helpers.toObject(content, (object) => {
      let image = {
        path: '/images/' + helpers.imageNameFormat(req.file.originalname),
        alt: req.body.altText // Decide how to implement Alt Tag
      };
      object.images.push(image);
      helpers.toMarkdown(object, (markdownFile) => {
        pagesManager.writeFile(filePath, markdownFile, () => {
          // On successful image upload - reload current page
          res.redirect(thisPath);
        });
      });
    });
  });
});

// Image Delete
app.delete('/edit/:type/:file/image', (req, res) => {
  const redirectPath = `/edit/${req.params.type}/${req.params.file}`;
  const filePath = path.join(siteRoot, `/src/${req.params.type}/${req.params.file}`);
  const imagePath = req.headers['image-path'];
  pagesManager.getFile(req.params.type, req.params.file, (file) => {
    helpers.toObject(file, (object) => {
      pagesManager.deleteImage(imagePath, () => {
        console.log('success - image deleted');
        object.images = object.images.filter((image) => {
          return image.path !== imagePath;
        });
        helpers.toMarkdown(object, (markdownFile) => {
          pagesManager.writeFile(filePath, markdownFile, () => {
            res.sendStatus(200);
          });
        });
      });
    });
  });
});

app.post('/edit/:type/:file', bodyParser.urlencoded({extended: true}), (req, res) => {
  const type = req.params.type;
  const file = req.params.file;
  const filePath = req.body.path;
  let pageObject = req.body;
  pagesManager.getFile(type, file, (content) => {
    helpers.toObject(content, (object) => {
      pageObject.images = object.images;
      helpers.toMarkdown(pageObject, (markdownFile) => {
        pagesManager.writeFile(filePath, markdownFile, () => {
          res.redirect('/');
        });
      });
    });
  });
});

// Delete Files
app.delete('/edit/:type/:file', (req, res) => {
  pagesManager.deleteFile(req.params, () => {
    console.log('File successfully deleted!');
    res.sendStatus(200);
  });
});


// Create Routes
app.get('/create/:type', (req, res) => {
  const type = req.params.type;
  if (type === 'pages' || type === 'posts') {
    getFiles(data => {
      pagesManager.getViews(viewsArray => {
        res.render('editor', {
          nav: {
            pages: data.pages,
            posts: data.posts
          },
          views: viewsArray,
          create: true,
          type: type
        });
      });
    });
  } else {
    res.redirect('/');
  }
});

app.post('/create/:type', bodyParser.urlencoded({extended: true}), (req, res) => {
  const formatMarkdownFileName = (titleString) => {
    return titleString.toLowerCase().split(' ').join('-') + '.md';
  };
  let fileName = formatMarkdownFileName(req.body.title);
  let filePath = siteRoot + '/src/' + req.params.type + '/' + fileName;
  helpers.toMarkdown(req.body, (markdownFile) => {
    pagesManager.writeFile(filePath, markdownFile, () => {
      res.redirect(`/edit/${req.params.type}/${fileName}`);
    });
  });
});


// Metalsmith Server Control Page

app.get('/metalsmith', (req, res) => {
  getFiles(data => {
    res.render('metalsmith', {
      nav: {
        pages: data.pages,
        posts: data.posts
      }
    });
  });
});



app.use(sassMiddleware({
   src: sassDir,
   dest: cssDir,
   outputStyle: 'compressed'
}));
app.use(express.static(__dirname + '/assets'));

// Handle 404 - page not found
app.use((req, res, next) => {
    res.status(404).render('404', {
      message: "Sorry, Page not found"
    })
});

app.listen(config.port, () => {
  console.log('Listening on port ' + config.port + '\nhttp://localhost:' + config.port);
});
