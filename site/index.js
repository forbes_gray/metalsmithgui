const Metalsmith  = require('metalsmith');
const collections = require('metalsmith-collections');
const layouts     = require('metalsmith-layouts');
const markdown    = require('metalsmith-markdown');
const permalinks  = require('metalsmith-permalinks');
const twig        = require('metalsmith-twig');
const serve       = require('metalsmith-serve');
const sass        = require('metalsmith-sass');
const snippet     = require('metalsmith-snippet');
const config      = require('../config.json');

console.log(config);

Metalsmith(__dirname)
  .metadata(config)
  .source('./src')
  .destination('./build')
  .clean(true)
  .use(collections({
    pages: {
      pattern: 'pages/*.md',
      sortBy: 'order'
    },
    posts: {
      pattern: 'posts/*.md',
      sortBy: 'date',
      reverse: true
    }
  }))
  .use(markdown())
  .use(snippet({
    maxLength: 200
  }))
  .use(permalinks({
    linksets: [{
      match: 'pages/index.md',
      pattern: ''
    },
    {
      match: {collection: 'pages'},
      pattern: ':title'
    },
    {
      match: {collection: 'posts'},
      pattern: 'posts/:title'
    }]
  }))
  .use(sass())
  .use(twig({
    directory: './views/templates',
    global: {
      metadata: this.metadata //Allow Twig Access to Metalsmith metadata
    }
  }))
  .build(err => {
    if (err) {
      console.log(err)
      throw err;
    }
  });
