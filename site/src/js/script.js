(() => {
  const overlay = document.querySelector('.overlay');
  window.onload = () => {
    overlay.classList.toggle('hidden');
  }

  const menuButton = document.querySelector('.menu');
  const navElement = document.querySelector('.main-nav');
  const openNav = (e) => {
    e.preventDefault();
    navElement.classList.toggle('open');
    menuButton.classList.toggle('open');
  };
  menuButton.addEventListener('click', openNav);

})();
