const express = require('express');
const server = express();
const open = require('open');
const port = process.env.PORT || 3001;

server.use(express.static(__dirname + '/build'));

server.listen(port, () => {
  console.log('Listening on port ' + port);
  open('http://localhost:' + port);
});
