/** Manage interaction with filesystem **/

const fs          = require('fs');
const path        = require('path');
const fm          = require('front-matter');

const siteRootDir  = path.join(__dirname, '../site');
const siteSettingsPath = path.join(__dirname, '../config.json');
const bitballoonSettingsPath = path.join(__dirname, '../config/bitballoon-client-options.json');

const pagesDir     = path.join(siteRootDir, '/src/pages/');
const postsDir     = path.join(siteRootDir, '/src/posts/');
const viewsDir     = path.join(siteRootDir, '/views/templates');

module.exports = {
  getIndex: () => {
    const indexPath = path.join(siteRootDir, '/src/index.md');
    const indexContent = fm(fs.readFileSync(indexPath, {encoding: 'utf8'}));
    return {
      title: indexContent.attributes.title,
      path: indexPath,
      file: 'index.md',
      content: indexContent.body
    };
  },
  getViews: (cb) => {
    fs.readdir(viewsDir, (err, data) => {
      if (err) throw new Error(err);
      const viewFiles = data.filter((file) => {
        return file.match(/.html/);
      }).map((file => {
        return {
          fileName: file,
          title: file.split('.')[0].split('-').join('-')
        }
      }));
      cb(viewFiles);
    })
  },
  getFiles: (cb) => {
    files = {
      pages: [],
      posts: []
    };
    fs.readdir(pagesDir, (err, pageData) => {
      if (err) throw new Error(err);
      let pagesArray = [];
      if (pageData) {
        pageData.forEach((page) => {
          const filePath = path.join(pagesDir, page);
          let pageContent = fm(fs.readFileSync(filePath, {encoding: 'utf8'}));
          pagesArray.push({
            title: pageContent.attributes.title,
            path: filePath,
            file: page,
            order: pageContent.attributes.order,
            content: pageContent.body
          });
        });
        // Sort pages using navigation order set in frontmatter
        files.pages = pagesArray.sort(function (a, b) {
          if (a.order < b.order) return -1;
          if (a.order > b.order) return 1;
          return 0;
        });
        fs.readdir(postsDir, (err, postData) => {
          if (err) throw new Error(err);
          let postsArray = [];
          if (postData) {
            postData.forEach((post) => {
              const filePath = path.join(postsDir, post);
              let postContent = fm(fs.readFileSync(filePath, {encoding: 'utf8'}));
              postsArray.push({
                title: postContent.attributes.title,
                path: filePath,
                file: post,
                date: postContent.attributes.date,
                content: postContent.body
              });
            });
            files.posts = postsArray.sort((a, b) => {
              if (a.data > b.date) return -1;
              if (a.date < b.date) return 1;
              return 0;
            });
          }
          cb(files);
        });
      };
    });
  },
  getFile: (type, file, cb) => {
    const filePath = path.join(siteRootDir, `/src/${type}/${file}`);
    fs.readFile(filePath, {encoding: 'utf8'}, (err, data) => {
      if (err) throw new Error(err);
      cb(data);
    });
  },
  writeFile: (path, data, cb) => {
    const filePath = path;
    fs.writeFile(filePath, data, (err) => {
      if (err) throw new Error(err);
      cb();
    });
  },
  deleteFile: (data, cb) => {
    // Data is object with type (page/post etc.) and file (filename)
    // Build path to file
    const filePath = path.join(siteRootDir, `/src/${data.type}/${data.file}`);
    fs.unlink(filePath, (err) => {
      if (err) console.log(err);
      cb();
    })
  },
  deleteImage: (imgPath, cb) => {
    // Need to remove reference to image from files frontmatter
    const imagePath = path.join(siteRootDir, `/src/${imgPath}`);
    fs.unlink(imagePath, (err) => {
      if (err) console.log(err);
      cb();
    });
  },
  getSettings: (cb) => {
    fs.readFile(siteSettingsPath, (err, file) => {
      if (err) throw new Error(err);
      fs.readFile(bitballoonSettingsPath, (err, bitballoonFile) => {
        if (err) throw new Error(err);
        let settingsObj = JSON.parse(file);
        settingsObj = JSON.parse(bitballoonFile).site_id;
        cb(settingsObj);
      });
    });
  },
  saveSiteSettings: (settings, cb) => {
    settingsObj = {
      siteName: settings.siteName,
      description: settings.description,
      author: {
        name: settings.name,
        bio: settings.bio,
        image: settings.image
      }
    };
    settingsJSON = JSON.stringify(settingsObj);
    fs.writeFile(siteSettingsPath, settingsJSON, (err) => {
      if (err) throw new Error(err);
      cb();
    })
  },
  getBitballoonSettings: (cb) => {
    fs.readFile(bitballoonSettingsPath, (err, file) => {
      if (err) throw new Error(err);
      const settingsObj = JSON.parse(file);
      cb(settingsObj);
    });
  },
  saveBitballoonSettings: (bitballoonConfig, cb) => {
    bitballoonConfigJSON = JSON.stringify(bitballoonConfig);
    fs.writeFile(bitballoonSettingsPath, bitballoonConfigJSON, (err) => {
      if (err) throw new Error(err);
      cb();
    })
  }
};
