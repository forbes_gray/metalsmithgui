const fm = require('front-matter');
const yamljs = require('yamljs');


// Take a markdown file and convert it to object
// Take page object and convert it to markdown (with valid yaml frontmatter)

module.exports = {
  toMarkdown: (object, cb) => {
    // Object passed should contain all required keys (title, view, imagesArray, content)
    // let fileObject = fm(object)
    console.log('toMarkdown \n', object);
    let frontmatter = {
      title: object.title,
      view: object.view,
      description: object.description,
      keywords: object.keywords,
      date: object.date,
      order: object.order,
      images: object.images,
      banner: object.banner
    };
    let yaml = yamljs.stringify(frontmatter);
    let body = object.body;
    let markdownFile = `---\r\n${yaml}---\r\n${body}`;
    cb(markdownFile);
  },
  toObject: (file, cb) => {
    // Passed the raw file - convert to JS Object
    console.log('toObject \n', file);
    let frontmatter = fm(file);
    let imagesArray = frontmatter.attributes.images ? frontmatter.attributes.images : [];
    let fileObject = {
      title: frontmatter.attributes.title,
      view: frontmatter.attributes.view,
      description: frontmatter.attributes.description,
      keywords: frontmatter.attributes.keywords,
      date: frontmatter.attributes.date,
      order: parseInt(frontmatter.attributes.order, 10),
      images: imagesArray,
      banner: frontmatter.attributes.banner,
      body: frontmatter.body
    }
    cb(fileObject);
  },
  imageNameFormat: (filename) => {
    // Reformat filename to remove special chars and replace whitespace with hyphen
    const fileExt = filename.match(/(.jpg)|(.png)|(.gif)|(.svg)|(.jpeg)+/g)[0];
    return filename.split(fileExt)[0].replace(/(?!\w|\s)./g, '').split(' ').join('-').concat(fileExt);
  }
};
